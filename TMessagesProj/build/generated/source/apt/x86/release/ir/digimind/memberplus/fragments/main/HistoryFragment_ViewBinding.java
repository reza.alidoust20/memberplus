// Generated code from Butter Knife. Do not modify!
package ir.digimind.memberplus.fragments.main;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import org.telegram.messenger.R;

public class HistoryFragment_ViewBinding<T extends HistoryFragment> implements Unbinder {
  protected T target;

  @UiThread
  public HistoryFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.recyclerHistory = Utils.findRequiredViewAsType(source, R.id.recyclerHistory, "field 'recyclerHistory'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.recyclerHistory = null;

    this.target = null;
  }
}
