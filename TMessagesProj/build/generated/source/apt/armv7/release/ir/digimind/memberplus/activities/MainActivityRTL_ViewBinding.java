// Generated code from Butter Knife. Do not modify!
package ir.digimind.memberplus.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.ivankocijan.magicviews.views.MagicTextView;
import java.lang.IllegalStateException;
import java.lang.Override;
import org.telegram.messenger.R;

public class MainActivityRTL_ViewBinding<T extends MainActivityRTL> implements Unbinder {
  protected T target;

  private View view2131558546;

  @UiThread
  public MainActivityRTL_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.drawer = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'drawer'", DrawerLayout.class);
    target.navigationView = Utils.findRequiredViewAsType(source, R.id.nav_view, "field 'navigationView'", NavigationView.class);
    target.menuButton = Utils.findRequiredViewAsType(source, R.id._continue, "field 'menuButton'", RelativeLayout.class);
    target.navigationListView = Utils.findRequiredViewAsType(source, R.id.listView, "field 'navigationListView'", ListView.class);
    target.fabView = Utils.findRequiredViewAsType(source, R.id.fab, "field 'fabView'", FloatingActionButton.class);
    target.coin = Utils.findRequiredViewAsType(source, R.id.coin, "field 'coin'", MagicTextView.class);
    view = Utils.findRequiredView(source, R.id.buy, "method 'onClick'");
    view2131558546 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.toolbar = null;
    target.drawer = null;
    target.navigationView = null;
    target.menuButton = null;
    target.navigationListView = null;
    target.fabView = null;
    target.coin = null;

    view2131558546.setOnClickListener(null);
    view2131558546 = null;

    this.target = null;
  }
}
