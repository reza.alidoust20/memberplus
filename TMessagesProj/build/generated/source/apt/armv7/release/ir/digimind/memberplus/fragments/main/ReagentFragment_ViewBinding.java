// Generated code from Butter Knife. Do not modify!
package ir.digimind.memberplus.fragments.main;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.ivankocijan.magicviews.views.MagicEditText;
import com.ivankocijan.magicviews.views.MagicTextView;
import java.lang.IllegalStateException;
import java.lang.Override;
import org.telegram.messenger.R;

public class ReagentFragment_ViewBinding<T extends ReagentFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ReagentFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.code = Utils.findRequiredViewAsType(source, R.id.code, "field 'code'", MagicEditText.class);
    target.supportTV = Utils.findRequiredViewAsType(source, R.id.supportTV, "field 'supportTV'", MagicTextView.class);
    target.yourCode = Utils.findRequiredViewAsType(source, R.id.yourCode, "field 'yourCode'", MagicTextView.class);
    target.register = Utils.findRequiredViewAsType(source, R.id.register, "field 'register'", CardView.class);
    target.copy = Utils.findRequiredViewAsType(source, R.id.copy, "field 'copy'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.code = null;
    target.supportTV = null;
    target.yourCode = null;
    target.register = null;
    target.copy = null;

    this.target = null;
  }
}
