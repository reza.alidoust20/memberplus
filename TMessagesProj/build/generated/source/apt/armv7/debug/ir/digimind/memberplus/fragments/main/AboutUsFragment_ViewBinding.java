// Generated code from Butter Knife. Do not modify!
package ir.digimind.memberplus.fragments.main;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import org.telegram.messenger.R;

public class AboutUsFragment_ViewBinding<T extends AboutUsFragment> implements Unbinder {
  protected T target;

  private View view2131558553;

  @UiThread
  public AboutUsFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.afeButton, "method 'onClick'");
    view2131558553 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (this.target == null) throw new IllegalStateException("Bindings already cleared.");

    view2131558553.setOnClickListener(null);
    view2131558553 = null;

    this.target = null;
  }
}
