// Generated code from Butter Knife. Do not modify!
package ir.digimind.memberplus.fragments.main;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.daprlabs.cardstack.SwipeDeck;
import com.ivankocijan.magicviews.views.MagicTextView;
import java.lang.IllegalStateException;
import java.lang.Override;
import org.telegram.messenger.R;

public class MainFragment_ViewBinding<T extends MainFragment> implements Unbinder {
  protected T target;

  @UiThread
  public MainFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.swipeDeck = Utils.findRequiredViewAsType(source, R.id.swipe_deck, "field 'swipeDeck'", SwipeDeck.class);
    target.textWarning = Utils.findRequiredViewAsType(source, R.id.textWarning, "field 'textWarning'", MagicTextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.swipeDeck = null;
    target.textWarning = null;

    this.target = null;
  }
}
