// Generated code from Butter Knife. Do not modify!
package ir.digimind.memberplus.fragments.main;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import org.telegram.messenger.R;

public class SupportFragment_ViewBinding<T extends SupportFragment> implements Unbinder {
  protected T target;

  @UiThread
  public SupportFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.supportTV = Utils.findRequiredViewAsType(source, R.id.supportTV, "field 'supportTV'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.supportTV = null;

    this.target = null;
  }
}
