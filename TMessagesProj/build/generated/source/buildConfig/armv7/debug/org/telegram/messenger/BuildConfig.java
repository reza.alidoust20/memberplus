/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.telegram.messenger;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "ir.digimind.memberplus";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "armv7";
  public static final int VERSION_CODE = 8515;
  public static final String VERSION_NAME = "1";
}
