/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.telegram.messenger;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "ir.digimind.memberplus";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "x86";
  public static final int VERSION_CODE = 8514;
  public static final String VERSION_NAME = "1";
}
