package ir.digimind.memberplus.fragments.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.telegram.messenger.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.rushorm.core.RushSearch;
import ir.digimind.memberplus.models.database.MyChannel;
import ir.digimind.memberplus.views.adapters.HistoryAdapter;
import ir.digimind.memberplus.views.adapters.VerticalLineDecorator;


public class HistoryFragment extends Fragment {

    @BindView(R.id.recyclerHistory)
    RecyclerView recyclerHistory;

    private GridLayoutManager lLayout;
    private List<MyChannel> channelModels;
    private HistoryAdapter historyAdapter;

    private int index = 0;


    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        channelModels = new ArrayList<MyChannel>();

        historyAdapter = new HistoryAdapter(getActivity(), channelModels);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        historyAdapter.setLoadMoreListener(new HistoryAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                recyclerHistory.post(new Runnable() {
                    @Override
                    public void run() {
                        index += 10;
                        loadMore(index);
                    }
                });
                //Calling loadMore function in Runnable to fix the
                // java.lang.IllegalStateException: Cannot call this method while RecyclerView is computing a layout or scrolling error
            }
        });
        recyclerHistory.setHasFixedSize(true);
        recyclerHistory.setLayoutManager(lLayout);
        recyclerHistory.addItemDecoration(new VerticalLineDecorator(2));
        recyclerHistory.setAdapter(historyAdapter);
        load(index);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        lLayout = new GridLayoutManager(getActivity(), 2);


    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    private void load(int index) {

        List<MyChannel> myChannels = new RushSearch().limit(10).offset(index)
                .find(MyChannel.class);
        channelModels.addAll(myChannels);
        historyAdapter.notifyDataChanged();
    }

    private void loadMore                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   (int index) {

        //add loading progress view
        channelModels.add(new MyChannel());
        historyAdapter.notifyItemInserted(channelModels.size() - 1);


        channelModels.remove(channelModels.size() - 1);

        List<MyChannel> myChannels = new RushSearch().limit(10).offset(index - 1)
                .find(MyChannel.class);
        if (myChannels.size() > 0) {
            channelModels.addAll(myChannels);

        } else {
            historyAdapter.setMoreDataAvailable(false);

        }

    }


}
