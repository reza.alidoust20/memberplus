package ir.digimind.memberplus.fragments.main;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daprlabs.cardstack.SwipeDeck;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.greenrobot.eventbus.EventBus;
import org.telegram.messenger.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.digimind.memberplus.models.buses.Coin;
import ir.digimind.memberplus.models.network.Channel;
import ir.digimind.memberplus.models.network.ListChannels;
import ir.digimind.memberplus.network.NetworkService;
import ir.digimind.memberplus.views.adapters.ChannelAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainFragment extends Fragment {


    @BindView(R.id.swipe_deck)
    SwipeDeck swipeDeck;
    @BindView(R.id.textWarning)
    MagicTextView textWarning;

    private ChannelAdapter channelAdapter;
    private KProgressHUD a;

    private int from = 0;
    private int to = 20;


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        a = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("صبر کنید");
//                .setDetailsLabel("Downloading data");
        a.setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeDeck.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {

            }

            @Override
            public void cardSwipedRight(int position) {

            }

            @Override
            public void cardsDepleted() {
                from += to;
                a.show();
                NetworkService.API().listChannels(from + "", to + "").enqueue(channelsCallback);


            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });

        try {
            a.show();
        } catch (Exception e) {

        }
        NetworkService.API().listChannels(from + "", to + "").enqueue(channelsCallback);


//
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private Callback<ListChannels> channelsCallback = new Callback<ListChannels>() {

        @Override
        public void onResponse(Call<ListChannels> call, Response<ListChannels> response) {
            try {
                a.dismiss();
            } catch (Exception o) {

            }
            EventBus.getDefault().post(new Coin());

            List<Channel> channelModels = response.body().getChannels();
            if (channelModels.size() == 0) {
                textWarning.setText("درحال حاضر کانالی برای نمایش وجود ندارد");
            }
            channelAdapter = new ChannelAdapter(getActivity(), channelModels);
            swipeDeck.setAdapter(channelAdapter);


            for (int i = 0; i < channelModels.size(); i++) {
//                Log.i("=======", ""+channelModels.get(i).getPerson());
            }
        }

        @Override
        public void onFailure(Call<ListChannels> call, Throwable t) {
//            Log.i("=======", "onFailure" + t.getMessage());
            a.dismiss();
            EventBus.getDefault().post(new Coin());

        }
    };


}
