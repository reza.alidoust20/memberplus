package ir.digimind.memberplus.fragments.main;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ivankocijan.magicviews.views.MagicEditText;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.telegram.messenger.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.digimind.memberplus.models.buses.Coin;
import ir.digimind.memberplus.models.network.Response;
import ir.digimind.memberplus.models.network.User;
import ir.digimind.memberplus.network.NetworkService;
import ir.digimind.memberplus.views.customs.CustomSnackBar;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReagentFragment extends Fragment {


    @BindView(R.id.code)
    MagicEditText code;
    @BindView(R.id.supportTV)
    MagicTextView supportTV;
    @BindView(R.id.yourCode)
    MagicTextView yourCode;
    @BindView(R.id.register)
    CardView register;
    @BindView(R.id.copy)
    CardView copy;

    public ReagentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reagent, container, false);
        ButterKnife.bind(this, view);
        final TelephonyManager mngr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);

        yourCode.setText("کد شما: " + mngr.getDeviceId());
        supportTV.setText(Html.fromHtml(
                " این برنامه را به دوستانتان معرفی کنید تا 4 سکه رایگان به شما هدیه داده شود "
                        + "<b/>"
                        + " اگر 20  نفر را به عنوان معرف اعلام کنید 40 سکه رایگان به شما هدیه داده خواهد شد "
                        + "<b/>"
                        + " شما می توانید دعوت نامه خود را به دوستانتان در تمام شبکه های اجتماعی بفرستید و این دعوت نامه شناسه شما عنوان معرف خواهد بود "
                        + "<b/>"
                        + " شمارهای خارج از کشور قابلیت ثبت معرف ندارند "


        ));
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Prefs.getBoolean("usedCode", false) && !code.getText().toString().equals(mngr.getDeviceId())) {
                    if (code.length() > 0) {
                        User user = new User();
                        user.setCoin(4);
                        final KProgressHUD progressHUD = KProgressHUD.create(getActivity())
                                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                                .setLabel("صبر کنید");
//                .setDetailsLabel("Downloading data");
                        progressHUD.setCancellable(true)
                                .setAnimationSpeed(2)
                                .setDimAmount(0.5f);
                        NetworkService.API().changeCoin(code.getText().toString(), user)
                                .enqueue(new Callback<Response>() {
                                    @Override
                                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                        progressHUD.dismiss();

                                        if (response.body().isResult()) {
                                            EventBus.getDefault().post(new Coin());
                                            CustomSnackBar snackBar = new CustomSnackBar(getActivity().getCurrentFocus(), "کد معرفی درست است.", Snackbar.LENGTH_LONG, Color.YELLOW);
                                            snackBar.showSnackBar();
                                            Prefs.putBoolean("usedCode", true);
                                        } else {
                                            CustomSnackBar snackBar = new CustomSnackBar(getActivity().getCurrentFocus(), "لطفا دوباره امتحان کنید", Snackbar.LENGTH_LONG, Color.YELLOW);
                                            snackBar.showSnackBar();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Response> call, Throwable t) {
                                        progressHUD.dismiss();
                                        CustomSnackBar snackBar = new CustomSnackBar(getActivity().getCurrentFocus(), "کد معرفی درست است.", Snackbar.LENGTH_LONG, Color.YELLOW);
                                        snackBar.showSnackBar();

                                    }
                                });

                    }
                } else {
                    CustomSnackBar snackBar = new CustomSnackBar(getActivity().getCurrentFocus(), "شما قبلا استفاده کرده اید.", Snackbar.LENGTH_LONG, Color.YELLOW);
                    snackBar.showSnackBar();
                }
            }
        });
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("کد", mngr.getDeviceId());
                clipboard.setPrimaryClip(clip);

                CustomSnackBar snackBar = new CustomSnackBar(getActivity().getCurrentFocus(), "کد کپی شد.", Snackbar.LENGTH_LONG, Color.YELLOW);
                snackBar.showSnackBar();

            }
        });
        return view;
    }


}
