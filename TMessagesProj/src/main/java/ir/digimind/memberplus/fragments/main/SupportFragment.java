package ir.digimind.memberplus.fragments.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.telegram.messenger.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class SupportFragment extends Fragment {


    @BindView(R.id.supportTV)
    TextView supportTV;

    public SupportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_support, container, false);
        ButterKnife.bind(this, view);

        final Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"infoakamtablighat@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "نظر و پیشنهاد اکام ممبر");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
        supportTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(i);
            }
        });
        supportTV.setText(Html.fromHtml(
                " شما می توانید از طریق ایمیل با ما در تماس باشید "
                        + "<b/>"
                        + " ما در کمتر از 24 ساعت به شما جواب خواهیم داد "
                        + "<b/>"
                        + " اگر به محدودیت تلگرام رسیده اید "
                        + "<b/>"
                        + " بدانید که محدودیت از طرف تلگرام است "
                        + "<b/>"
                        + " ما نمی توانیم مشکل را برطرف کنیم "
                        + "<b/>"
                        + " ایمیل: infoakamtablighat@gmail.com"


        ));
        return view;

    }

}
