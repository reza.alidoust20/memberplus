package ir.digimind.memberplus.fragments.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.telegram.messenger.R;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class AboutUsFragment extends Fragment {


    public AboutUsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_about_us, container,
                false);
        ButterKnife.bind(this, root);
        return root;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @OnClick(R.id.afeButton)
    public void onClick() {
        String uri = "http://digimind.ir/";
        Intent intent3 = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent3);
    }
}
