package ir.digimind.memberplus.models.network;

/**
 * MemberPlus
 * Created by Reza alidoust on 11/1/16.
 * AFE
 * rezaalidoust@live.com
 */

public class Response {
    private boolean result;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
