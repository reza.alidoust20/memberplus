package ir.digimind.memberplus.models.network;

/**
 * MemberPlus
 * Created by Reza alidoust on 11/26/16.
 * AFE
 * rezaalidoust@live.com
 */

public class User {
    private String tel;
    private int coin;

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }
}
