package ir.digimind.memberplus.models.buses;

/**
 * MemberPlus
 * Created by Reza alidoust on 10/23/16.
 * AFE
 * rezaalidoust@live.com
 */

public class Channel {
    private int channelUser;

    public Channel(int channelUser){
        this.channelUser = channelUser;
    }

    public int getChannelUser() {
        return channelUser;
    }

    public void setChannelUser(int channelUser) {
        this.channelUser = channelUser;
    }
}
