package ir.digimind.memberplus.models.network;

import java.util.ArrayList;
import java.util.List;

/**
 * MemberPlus
 * Created by Reza alidoust on 11/1/16.
 * AFE
 * rezaalidoust@live.com
 */

public class ListChannels {
    private Boolean result;
    private List<Channel> channels = new ArrayList<Channel>();

    /**
     * @return The result
     */
    public Boolean getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Boolean result) {
        this.result = result;
    }

    /**
     * @return The channels
     */
    public List<Channel> getChannels() {
        return channels;
    }

    /**
     * @param channels The channels
     */
    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }

}