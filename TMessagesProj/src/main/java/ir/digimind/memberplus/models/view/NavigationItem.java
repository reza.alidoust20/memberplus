package ir.digimind.memberplus.models.view;

import android.view.View;

/**
 * Created by Technovibe on 17-04-2015.
 */
public abstract class NavigationItem {
    private String title;
    private Integer pic_id;
    private Integer pic_id_pressed;
    private View.OnClickListener onClickListener;

    public NavigationItem(String title, Integer pic_id, Integer pic_id_pressed) {
        this.pic_id = pic_id;
        this.title = title;
        this.pic_id_pressed = pic_id_pressed;
        onClickListener = onClickListener();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Integer getPic_id() {
        return pic_id;
    }

    public void setPic_id(Integer pic_id) {
        this.pic_id = pic_id;
    }

    public Integer getPic_id_pressed() {
        return pic_id_pressed;
    }

    public void setPic_id_pressed(Integer pic_id_pressed) {
        this.pic_id_pressed = pic_id_pressed;
    }

    public abstract View.OnClickListener onClickListener();

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}
