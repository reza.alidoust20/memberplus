package ir.digimind.memberplus.models.network;

/**
 * MemberPlus
 * Created by Reza alidoust on 11/26/16.
 * AFE
 * rezaalidoust@live.com
 */

public class ResponseUser {
    private boolean result;
    private User user;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
