package ir.digimind.memberplus.models.network;

/**
 * MemberPlus
 * Created by Reza alidoust on 11/1/16.
 * AFE
 * rezaalidoust@live.com
 */

public class Channel {

    private String username;
    private String channel_id;
    private String name;
    private int person;
    private String desc;

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The channel_id
     */
    public String getChannel_id() {
        return channel_id;
    }

    /**
     *
     * @param channel_id
     * The channel_id
     */
    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The person
     */
    public int getPerson() {
        return person;
    }

    /**
     *
     * @param person
     * The person
     */
    public void setPerson(int person) {
        this.person = person;
    }

    /**
     *
     * @return
     * The desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     *
     * @param desc
     * The desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

}
