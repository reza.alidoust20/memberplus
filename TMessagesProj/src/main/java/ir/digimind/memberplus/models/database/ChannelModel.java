package ir.digimind.memberplus.models.database;

/**
 * AirPush
 * Created by Reza alidoust on 10/26/16.
 * AFE
 * rezaalidoust@live.com
 */

public class ChannelModel {
    private String username;
    private String userId;
    private String name;
    private String img;
    private int star;
    private String description;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
