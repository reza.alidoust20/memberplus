package ir.digimind.memberplus.network;

import ir.digimind.memberplus.models.network.Channel;
import ir.digimind.memberplus.models.network.ListChannels;
import ir.digimind.memberplus.models.network.Response;
import ir.digimind.memberplus.models.network.ResponseUser;
import ir.digimind.memberplus.models.network.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * MemberPlus
 * Created by Reza alidoust on 11/1/16.
 * AFE
 * rezaalidoust@live.com
 */

public interface NetworkInterface {
    @GET("channel/{from}/{to}")
    Call<ListChannels> listChannels(@Path("from") String from, @Path("to") String to);

    @POST("channel/{tel}")
    Call<Response> addChannel(@Path("tel") String tel, @Body Channel channel);


    @PATCH("channel/{username}")
    Call<Response> joinToChannel(@Path("username") String username, @Body User user);

    @PATCH("channel/report/{username}")
    Call<Response> reportChannel(@Path("username") String username);

    @POST("user")
    Call<Response> addUser(@Body User user);

    @PATCH("user/{tel}")
    Call<Response> changeCoin(@Path("tel") String tel, @Body User user);

    @GET("user/{tel}")
    Call<ResponseUser> getUser(@Path("tel") String tel);


}
