package ir.digimind.memberplus.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * MemberPlus
 * Created by Reza alidoust on 11/1/16.
 * AFE
 * rezaalidoust@live.com
 */

public class NetworkService {


    public static NetworkInterface API() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)

                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.membertelegramapi.ir/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NetworkInterface service = retrofit.create(NetworkInterface.class);
        return service;
        // baraye estefade az web service
        // NetworkService.API.{esme tavabe dar NetworkInterfaces}

    }
}
