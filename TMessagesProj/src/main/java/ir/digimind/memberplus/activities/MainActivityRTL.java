package ir.digimind.memberplus.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.android.trivialdrivesample.util.IabHelper;
import com.example.android.trivialdrivesample.util.IabResult;
import com.example.android.trivialdrivesample.util.Purchase;
import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicEditText;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.pixplicity.easyprefs.library.Prefs;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.telegram.messenger.R;
import org.telegram.tgnet.TLRPC;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.uk.rushorm.core.RushSearch;
import ir.digimind.memberplus.fragments.main.AboutUsFragment;
import ir.digimind.memberplus.fragments.main.HistoryFragment;
import ir.digimind.memberplus.fragments.main.MainFragment;
import ir.digimind.memberplus.fragments.main.ReagentFragment;
import ir.digimind.memberplus.fragments.main.SupportFragment;
import ir.digimind.memberplus.models.buses.Coin;
import ir.digimind.memberplus.models.database.JoinedChannel;
import ir.digimind.memberplus.models.database.MyChannel;
import ir.digimind.memberplus.models.network.Channel;
import ir.digimind.memberplus.models.network.Response;
import ir.digimind.memberplus.models.network.ResponseUser;
import ir.digimind.memberplus.models.network.User;
import ir.digimind.memberplus.models.view.NavigationItem;
import ir.digimind.memberplus.network.NetworkService;
import ir.digimind.memberplus.utils.TL_Utile;
import ir.digimind.memberplus.utils.Tools;
import ir.digimind.memberplus.views.PersianNumber;
import ir.digimind.memberplus.views.adapters.NavigationAdapter;
import ir.digimind.memberplus.views.customs.CustomSnackBar;
import ir.tapsell.tapsellvideosdk.developer.DeveloperInterface;
import retrofit2.Call;
import retrofit2.Callback;


public class MainActivityRTL extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id._continue)
    RelativeLayout menuButton;

    @BindView(R.id.listView)
    ListView navigationListView;
    @BindView(R.id.fab)
    FloatingActionButton fabView;
    @BindView(R.id.coin)
    MagicTextView coin;

    private ArrayList<NavigationItem> navigationItems;
    private String tel;


    private NavigationAdapter navigationAdapter;

    private FragmentManager fragmentManager = getSupportFragmentManager();
    private Fragment fragment;
    private String titleChannel;

    public void getAllChannel() {
        Log.i("channel", "start gettALLCHannel");

        TL_Utile.getChannels(new TL_Utile.onGetChannelsComplete() {
            @Override
            public void onSuccess(ArrayList<TLRPC.Chat> chats) {
                Log.i("channel", "onSuccess gettALLCHannel");

                final List<JoinedChannel> joinedChannels = new RushSearch()
                        .find(JoinedChannel.class);
                int count = 0;
                final ArrayList<Integer> removedChannel = new ArrayList<Integer>();
                for (int i = 0; i < joinedChannels.size(); i++) {
                    Log.i("joined ch" + i, joinedChannels.get(i).getUsername());
                    boolean b = contain(chats, joinedChannels.get(i));
                    if (b) {
                        Log.i("channel" + i, "contain");

                    } else {
                        Log.i("channel" + i, "not contain");

                    }
                    if (!b) {
                        count++;
                        removedChannel.add(i);
                    }
                }
                if (count > 0) {
                    User user = new User();
                    user.setCoin(-1 * count);
                    final int finalCount = count;
                    NetworkService.API().changeCoin(tel, user).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            if (response.body().isResult()) {
                                for (int i = 0; i < removedChannel.size(); i++) {
                                    joinedChannels.get(removedChannel.get(i)).delete();
                                }
                                CustomSnackBar snackBar = new CustomSnackBar(fabView, "شما " + PersianNumber.toPersianNumber("" + finalCount) + " کانال را ترک کردید", Snackbar.LENGTH_LONG, Color.YELLOW);
                                snackBar.showSnackBar();
                                EventBus.getDefault().post(new Coin());

                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {

                        }
                    });

                }
            }

            boolean contain(ArrayList<TLRPC.Chat> chats, JoinedChannel joinedChannel) {
                for (int j = 0; j < chats.size(); j++) {
                    if (joinedChannel.getUsername().trim().equals(chats.get(j).username.trim())) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onFail() {
                Log.i("channel", "onFail gettALLCHannel");


            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_rtl_navigation);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ButterKnife.bind(this);
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        DeveloperInterface.getInstance(MainActivityRTL.this).init(
                "kfbgbofdbaknlfgotbnmcmleoghsognjiosioddenichrfapfcidghboqteibffldifnno"
                , MainActivityRTL.this);


        tel = mngr.getDeviceId();

        if (Prefs.getBoolean("firstLogin", true)) {
            User user = new User();
            user.setTel(tel);
            a = KProgressHUD.create(MainActivityRTL.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("صبر کنید");
//                .setDetailsLabel("Downloading data");
            a.setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
            a.setCancellable(true);
//            a.show();

            NetworkService.API().addUser(user).enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    a.dismiss();

                    Prefs.putBoolean("firstLogin", false);

                }

                @Override
                public void onFailure(Call<Response> call, Throwable t) {
                    a.dismiss();

                }
            });
        }


        setSupportActionBar(toolbar);
        initIab();
//        Prefs.putInt(PrefsKeys.KEY_COINS, 1000);
//        Prefs.putInt(PrefsKeys.KEY_COINS, Prefs.getInt(PrefsKeys.KEY_COINS, 1000));
        fabView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddChannel();

            }
        });
        getAllChannel();


        navigationItems = new ArrayList<NavigationItem>();

        NavigationItem menuItem = new NavigationItem(
                getString(R.string.navigation_main)
                , R.drawable.ic_home_black_24dp
                , R.drawable.ic_home_amber_900_24dp) {
            @Override
            public View.OnClickListener onClickListener() {
                View.OnClickListener clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragment = new MainFragment();

                        fragmentManager.beginTransaction()
                                .replace(R.id.container, fragment).commit();
                        drawer.closeDrawer(GravityCompat.END);
                    }
                };
                return clickListener;
            }
        };
        navigationItems.add(menuItem);
        menuItem = new NavigationItem(
                getString(R.string.navigation_buy)
                , R.drawable.ic_local_grocery_store_black_24dp
                , R.drawable.ic_local_grocery_store_yellow_900_24dp) {
            @Override
            public View.OnClickListener onClickListener() {
                View.OnClickListener clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        drawer.closeDrawer(GravityCompat.END);

                        dialogBuy();


                    }
                };
                return clickListener;
            }
        };
        navigationItems.add(menuItem);
        menuItem = new NavigationItem(
                getString(R.string.navigation_history)
                , R.drawable.ic_history_black_24dp
                , R.drawable.ic_history_yellow_900_24dp) {
            @Override
            public View.OnClickListener onClickListener() {
                View.OnClickListener clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragment = new HistoryFragment();

                        fragmentManager.beginTransaction()
                                .replace(R.id.container, fragment).commit();
                        drawer.closeDrawer(GravityCompat.END);
                    }
                };
                return clickListener;
            }
        };
        navigationItems.add(menuItem);
        menuItem = new NavigationItem(
                getString(R.string.navigation_setting)
                , R.drawable.ic_settings_black_24dp
                , R.drawable.ic_settings_yellow_900_24dp) {
            @Override
            public View.OnClickListener onClickListener() {
                View.OnClickListener clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragment = new ReagentFragment();

                        fragmentManager.beginTransaction()
                                .replace(R.id.container, fragment).commit();
                        drawer.closeDrawer(GravityCompat.END);
                    }
                };
                return clickListener;
            }
        };
        navigationItems.add(menuItem);

        menuItem = new NavigationItem(
                getString(R.string.navigation_ads)
                , R.drawable.ic_show_chart_black_24dp
                , R.drawable.ic_show_chart_yellow_900_24dp) {
            @Override
            public View.OnClickListener onClickListener() {
                View.OnClickListener clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DeveloperInterface.getInstance(MainActivityRTL.this)
                                .showNewVideo(MainActivityRTL.this,
                                        DeveloperInterface.TAPSELL_DIRECT_ADD_REQUEST_CODE,
                                        0,
                                        DeveloperInterface.VideoPlay_TYPE_SKIPPABLE);
                    }
                };
                return clickListener;
            }
        };
        navigationItems.add(menuItem);


        menuItem = new NavigationItem(
                getString(R.string.navigation_share)
                , R.drawable.ic_share_black_24dp
                , R.drawable.ic_share_yellow_900_24dp) {
            @Override
            public View.OnClickListener onClickListener() {
                View.OnClickListener clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Tools.shareApplication(MainActivityRTL.this);
                        drawer.closeDrawer(GravityCompat.END);

                    }
                };
                return clickListener;
            }
        };
        navigationItems.add(menuItem);

        menuItem = new NavigationItem(
                getString(R.string.navigation_about_us)
                , R.drawable.ic_info_outline_black_24dp
                , R.drawable.ic_info_outline_yellow_900_24dp) {
            @Override
            public View.OnClickListener onClickListener() {
                View.OnClickListener clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragment = new AboutUsFragment();

                        fragmentManager.beginTransaction()
                                .replace(R.id.container, fragment).commit();
                        drawer.closeDrawer(GravityCompat.END);
                    }
                };
                return clickListener;
            }
        };
//        navigationItems.add(menuItem);

        menuItem = new NavigationItem(
                getString(R.string.navigation_contact_us)
                , R.drawable.ic_comment_black_24dp
                , R.drawable.ic_comment_yellow_900_24dp) {
            @Override
            public View.OnClickListener onClickListener() {
                View.OnClickListener clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fragment = new SupportFragment();

                        fragmentManager.beginTransaction()
                                .replace(R.id.container, fragment).commit();
                        drawer.closeDrawer(GravityCompat.END);
                    }
                };
                return clickListener;
            }
        };
        navigationItems.add(menuItem);
        menuItem = new NavigationItem(
                getString(R.string.navigation_exit)
                , R.drawable.ic_exit_to_app_black_24dp
                , R.drawable.ic_exit_to_app_yellow_900_24dp) {
            @Override
            public View.OnClickListener onClickListener() {
                View.OnClickListener clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                };
                return clickListener;
            }
        };
        navigationItems.add(menuItem);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            navigationListView.setVerticalScrollbarPosition(View.SCROLLBAR_POSITION_LEFT);
        }
        navigationAdapter = new NavigationAdapter(MainActivityRTL.this, navigationItems, fragmentManager);
        navigationView.setNavigationItemSelectedListener(this);

        navigationListView.setAdapter(navigationAdapter);


        navigationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String title = ((NavigationItem) navigationListView.getAdapter().getItem(i)).getTitle();
                if (title.equals(getString(R.string.navigation_about_us))) {

                } else if (title.equals(getString(R.string.navigation_contact_us))) {

                } else if (title.equals(getString(R.string.navigation_setting))) {
//                    startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                } else if (title.equals(getString(R.string.navigation_exit))) {

                } else if (title.equals(getString(R.string.navigation_share))) {
                    Tools.shareApplication(MainActivityRTL.this);
                }
                drawer.closeDrawer(GravityCompat.END);
            }
        });
        fragment = new MainFragment();

        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment).commit();

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT);
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }


    @OnClick(R.id.buy)
    public void onClick() {
        dialogBuy();


    }

    private void dialogBuy()

    {
        Dialog dialog = new Dialog(MainActivityRTL.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_buy);
        final MagicTextView personNum = (MagicTextView) dialog.findViewById(R.id.personNumber);
        final MagicTextView cost = (MagicTextView) dialog.findViewById(R.id.cost);
        final DiscreteSeekBar seekBar = (DiscreteSeekBar) dialog.findViewById(R.id.seekBar);
        final String Prices[] = {"۵۰۰", "۱٬۰۰۰", "۲٬۰۰۰", "۵٬۰۰۰", "۱۰٬۰۰۰", "۱۸٬۰۰۰"};
        final int seke[] = {35, 70, 140, 350, 700, 1300};

        seekBar.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
            @Override
            public int transform(int value) {

                personNum.setText(PersianNumber.toPersianNumber(Integer.toString(seke[value - 1])) + "سکه");
                cost.setText(PersianNumber.toPersianNumber(Prices[value - 1]) + "تومان");

                return value;
            }
        });
        final MagicButton buy = (MagicButton) dialog.findViewById(R.id.buy);
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int coins = seekBar.getProgress();
                switch (coins) {

                    case 1:
                        buy(SKU_CONSOME0);
                        break;
                    case 2:
                        buy(SKU_CONSOME1);
                        break;
                    case 3:
                        buy(SKU_CONSOME2);
                        break;
                    case 4:
                        buy(SKU_CONSOME3);
                        break;
                    case 5:
                        buy(SKU_CONSOME4);
                        break;
                    case 6:
                        buy(SKU_CONSOME5);
                        break;
                }
                Toast.makeText(MainActivityRTL.this, coins + "", Toast.LENGTH_SHORT).show();
            }
        });
//        Bitmap map = takeScreenShot(MainActivityRTL.this);
//        Bitmap fast = fastBlur(map, 10);
//        final Drawable draw = new BitmapDrawable(getResources(), fast);
//        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
        //00968C
    }

    final int member[] = {15, 30, 50, 75, 150, 300};
    final int seke[] = {30, 60, 100, 150, 300, 500};
    int costValue = seke[0];


    //pardakht daroon barnameiii=================================
    // Debug tag, for logging
    static final String TAG = "Masrafi";
    // SKUs for our consome products
    final String SKU_CONSOME0 = "seke_50";
    final String SKU_CONSOME1 = "seke_100";
    final String SKU_CONSOME2 = "seke_200";
    final String SKU_CONSOME3 = "seke_300";
    final String SKU_CONSOME4 = "seke_700";
    final String SKU_CONSOME5 = "seke_1300";
    final int RC_REQUEST = 10001;
    IabHelper mHelper;
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener;
    private KProgressHUD a;

    public void initIab() {
        String publicKey = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwDk8wi83bPGfKeb/GUhJ1TccXlzL6pq9C5CyPRipMwQSyFW+YBwbnd9eSH2bgwfwB7fZqAWOcWdX0sedMdhZMHFoe0yOvYe0egti7DQlXqTWhOQlipcRxsvj26DbAXXhFWDxXEgplUHMEmPYH+ByPSGd/2qdKDDn4xnG72g3odUNxCMDlDhZyqQDcczN4evcnBc1tbz15Lv3wsPpaG0Pk0cpIusDPsFxnk65WXJmRcCAwEAAQ==";
        mHelper = new IabHelper(this, publicKey);
        mHelper.enableDebugLogging(false, "wwwwwwwwwww");
        mHelper.startSetup(new
                                   IabHelper.OnIabSetupFinishedListener() {
                                       public void onIabSetupFinished(IabResult result) {
                                           if (!result.isSuccess()) {
                                               Log.d(TAG, "In-app Billing setup failed: " +
                                                       result);
                                           } else {
                                               Log.d(TAG, "In-app Billing is set up OK");
                                           }
                                       }
                                   });
        a = KProgressHUD.create(MainActivityRTL.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("صبر کنید");
//                .setDetailsLabel("Downloading data");
        a.setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {

                if (result.isFailure()) {

                    Toast.makeText(getApplicationContext(), "در خرید از بازار مشکلی پیش آمد", Toast.LENGTH_SHORT).show();
                    a.dismiss();
                    return;
                }
                mHelper.consumeAsync(purchase,
                        mConsumeFinishedListener);
            }
        };


        mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
            public void onConsumeFinished(Purchase purchase, IabResult result) {

                if (result.isSuccess()) {
                    updateUI(purchase.getSku());
                    a.dismiss();

                } else {
                    a.dismiss();
                    Toast.makeText(getApplicationContext(), "ابتدا باید سکه خریده شود", Toast.LENGTH_SHORT).show();
                }
            }

        };
    }

    private void dialogAddChannel() {
        final Dialog dialog = new Dialog(MainActivityRTL.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_one);
        final MagicEditText username = (MagicEditText) dialog.findViewById(R.id.username);
        final MagicButton _continue = (MagicButton) dialog.findViewById(R.id._continue);
        _continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString() != null && !username.getText().toString().equals("")) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    final Dialog dialog = new Dialog(MainActivityRTL.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_add_two);
                    final MagicTextView name = (MagicTextView) dialog.findViewById(R.id.name);
                    final MagicTextView personNum = (MagicTextView) dialog.findViewById(R.id.personNumber);
                    final MagicTextView cost = (MagicTextView) dialog.findViewById(R.id.cost);
                    final MagicButton add = (MagicButton) dialog.findViewById(R.id.add);
                    final DiscreteSeekBar seekBar = (DiscreteSeekBar) dialog.findViewById(R.id.seekBar);
                    seekBar.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
                        @Override
                        public int transform(int value) {
                            personNum.setText(PersianNumber.toPersianNumber(Integer.toString(member[value - 1])) + "عضو");
                            cost.setText(PersianNumber.toPersianNumber(Integer.toString(seke[value - 1])) + "سکه");
                            costValue = seke[value - 1];
                            return value;
                        }
                    });
                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            final Channel channel = new Channel();
                            channel.setUsername(username.getText().toString());
                            channel.setPerson(member[seekBar.getProgress()]);

                            NetworkService.API().addChannel(tel, channel).enqueue(new Callback<Response>() {
                                @Override
                                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                    if (response.body().isResult()) {
                                        List<MyChannel> myChannels = new RushSearch()
                                                .whereEqual("username", username.getText().toString())
                                                .limit(1)
                                                .find(MyChannel.class);
                                        if (myChannels.size() > 0) {
                                            myChannels.get(0).setNumber
                                                    (myChannels.get(0).getNumber() + channel.getPerson());
                                            myChannels.get(0).setTitle(titleChannel);
                                            myChannels.get(0).save();
                                            // ghablan vojod dashte channel
                                        } else {
                                            MyChannel myChannel = new MyChannel();
                                            myChannel.setUsername(username.getText().toString());
                                            myChannel.setNumber(channel.getPerson());
                                            myChannel.setTitle(titleChannel);
                                            myChannel.save();
                                            //channel jadid
                                        }
                                        CustomSnackBar snackBar = new CustomSnackBar(fabView, "کانال شما افزوده شد", Snackbar.LENGTH_LONG, Color.YELLOW);
                                        snackBar.showSnackBar();
                                        EventBus.getDefault().post(new Coin());
                                    } else {
                                        CustomSnackBar snackBar = new CustomSnackBar(fabView, "شما به تعداد لازم سکه ندارید", Snackbar.LENGTH_LONG, Color.YELLOW);
                                        snackBar.showSnackBar();
                                        dialogBuy();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Response> call, Throwable t) {
                                    CustomSnackBar snackBar = new CustomSnackBar(fabView, "مشکل در برقراری ارتباط با سرور", Snackbar.LENGTH_LONG, Color.YELLOW);
                                    snackBar.showSnackBar();
                                }
                            });

                        }
                    });
                    a = KProgressHUD.create(MainActivityRTL.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setLabel("صبر کنید");
//                .setDetailsLabel("Downloading data");
                    a.setCancellable(false)
                            .setAnimationSpeed(2)
                            .setDimAmount(0.5f);
                    a.setCancellable(true);
                    a.show();
                    TL_Utile.getChannelId(username.getText().toString(), new TL_Utile.onGetChatIdComplete() {
                        @Override
                        public void onSuccess(TLRPC.Chat chat, int id, String title) {
                            a.dismiss();
                            titleChannel = title;
                            name.setText("کانال" + " " + title);
//                            Bitmap map = takeScreenShot(MainActivityRTL.this);
//                            Bitmap fast = fastBlur(map, 10);
//                            final Drawable draw = new BitmapDrawable(getResources(), fast);
//                            dialog.getWindow().setBackgroundDrawable(draw);
                            dialog.show();
                        }

                        @Override
                        public void onFail() {
                            a.dismiss();
                            CustomSnackBar snackBar = new CustomSnackBar(fabView, "کانال وارد شده وجود ندارد.", Snackbar.LENGTH_LONG, Color.YELLOW);
                            snackBar.showSnackBar();
                            dialog.dismiss();
                        }
                    });
                }
            }
        });
//        Bitmap map = takeScreenShot(MainActivityRTL.this);
//        Bitmap fast = fastBlur(map, 10);
//        final Drawable draw = new BitmapDrawable(getResources(), fast);
//        dialog.getWindow().setBackgroundDrawable(draw);
        dialog.show();
        //00968C
    }


    public void buy(String Sku) {
        try {
            mHelper.launchPurchaseFlow(MainActivityRTL.this, Sku, RC_REQUEST, mPurchaseFinishedListener);
            a.show();
        } catch (Exception e) {
            Toast.makeText(MainActivityRTL.this, "خطا", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateUI(String SKU) {
        int addedCoin = 0;
        if (SKU.equals(SKU_CONSOME0)) {
            addedCoin = 35;
            Toast.makeText(MainActivityRTL.this, "به شما 35 سکه اضافه شد", Toast.LENGTH_SHORT).show();
        } else if (SKU.equals(SKU_CONSOME1)) {
            addedCoin = 70;
            Toast.makeText(MainActivityRTL.this, "به شما 70 سکه اضافه شد", Toast.LENGTH_SHORT).show();
        } else if (SKU.equals(SKU_CONSOME2)) {
            addedCoin = 140;
            Toast.makeText(MainActivityRTL.this, "به شما 140 سکه اضافه شد", Toast.LENGTH_SHORT).show();
        } else if (SKU.equals(SKU_CONSOME3)) {
            addedCoin = 350;
            Toast.makeText(MainActivityRTL.this, "به شما 350 سکه اضافه شد", Toast.LENGTH_SHORT).show();
        } else if (SKU.equals(SKU_CONSOME4)) {
            addedCoin = 700;
            Toast.makeText(MainActivityRTL.this, "به شما 700 سکه اضافه شد", Toast.LENGTH_SHORT).show();
        } else if (SKU.equals(SKU_CONSOME5)) {
            addedCoin = 1300;
            Toast.makeText(MainActivityRTL.this, "به شما 1300 سکه اضافه شد", Toast.LENGTH_SHORT).show();
        }
        User user = new User();
        user.setCoin(addedCoin);
        NetworkService.API().changeCoin(tel, user).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.body().isResult()) {
                    EventBus.getDefault().post(new Coin());

                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        try {
            if (requestCode == DeveloperInterface.TAPSELL_DIRECT_ADD_REQUEST_CODE) {
                Log.i("reza", data.toString());
                Log.i("reza", "1 = " + data
                        .hasExtra(DeveloperInterface.TAPSELL_DIRECT_CONNECTED_RESPONSE));
                Log.i("reza", "2 = " + data
                        .hasExtra(DeveloperInterface.TAPSELL_DIRECT_AVAILABLE_RESPONSE));
                Log.i("reza", "3 = " + data
                        .hasExtra(DeveloperInterface.TAPSELL_DIRECT_AWARD_RESPONSE));
                Log.i("reza", "1 = " + data
                        .getBooleanExtra(DeveloperInterface.TAPSELL_DIRECT_CONNECTED_RESPONSE, false));
                Log.i("reza", "2 = " + data
                        .getBooleanExtra(DeveloperInterface.TAPSELL_DIRECT_AVAILABLE_RESPONSE, false));
                Log.i("reza", "3 = " + data
                        .getIntExtra(DeveloperInterface.TAPSELL_DIRECT_AWARD_RESPONSE, 1));

                if (data
                        .getIntExtra(DeveloperInterface.TAPSELL_DIRECT_AWARD_RESPONSE, 1) > 0) {
                    User user = new User();
                    user.setCoin(3);
                    NetworkService.API().changeCoin(tel, user).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            if (response.body().isResult()) {

                                CustomSnackBar snackBar = new CustomSnackBar(fabView, "۳ سکه به شما افزوده شد", Snackbar.LENGTH_LONG, Color.YELLOW);
                                snackBar.showSnackBar();
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {

                        }
                    });

                    EventBus.getDefault().post(new Coin());
                } else {
                    CustomSnackBar snackBar = new CustomSnackBar(fabView, "تبلیغ کامل نشده است.", Snackbar.LENGTH_LONG, Color.YELLOW);
                    snackBar.showSnackBar();
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }


    //pardakht daroon barnameiii=================================
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Coin coi) {
        if (coi != null) {
            Call<ResponseUser> user = NetworkService.API().getUser(tel);

//            final KProgressHUD progressHUD = KProgressHUD.create(MainActivityRTL.this)
//                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                    .setLabel("صبر کنید");
////                .setDetailsLabel("Downloading data");
//            progressHUD.setCancellable(true)
//                    .setAnimationSpeed(2)
//                    .setDimAmount(0.5f);
            user.enqueue(new Callback<ResponseUser>() {

                @Override
                public void onResponse(Call<ResponseUser> call, retrofit2.Response<ResponseUser> response) {
                    if (response.body().isResult()) {
                        coin.setText(PersianNumber.toPersianNumber("" + response.body().getUser().getCoin()));
                    }
//                    progressHUD.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseUser> call, Throwable t) {
//                    progressHUD.dismiss();
                    CustomSnackBar snackBar = new CustomSnackBar(fabView, "مشکل در برقراری ارتباط با سرور", Snackbar.LENGTH_LONG, Color.YELLOW);
                    snackBar.showSnackBar();
                }
            });

        }

    }

    @Override
    protected void onStart() {

        super.onStart();
        EventBus.getDefault().register(this);


    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);


    }


}
