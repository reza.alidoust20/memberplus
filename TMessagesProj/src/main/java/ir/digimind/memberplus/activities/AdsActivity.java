package ir.digimind.memberplus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.telegram.messenger.R;

import ir.tapsell.tapsellvideosdk.developer.DeveloperInterface;


public class AdsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);
        DeveloperInterface.getInstance(AdsActivity.this)
                .showNewVideo(AdsActivity.this,
                        DeveloperInterface.TAPSELL_DIRECT_ADD_REQUEST_CODE,
                        -2,
                        DeveloperInterface.VideoPlay_TYPE_SKIPPABLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == DeveloperInterface.TAPSELL_DIRECT_ADD_REQUEST_CODE) {
                Log.i("reza", data.toString());
                Log.i("reza", "1 = " + data
                        .hasExtra(DeveloperInterface.TAPSELL_DIRECT_CONNECTED_RESPONSE));
                Log.i("reza", "2 = " + data
                        .hasExtra(DeveloperInterface.TAPSELL_DIRECT_AVAILABLE_RESPONSE));
                Log.i("reza", "3 = " + data
                        .hasExtra(DeveloperInterface.TAPSELL_DIRECT_AWARD_RESPONSE));
                Log.i("reza", "1 = " + data
                        .getBooleanExtra(DeveloperInterface.TAPSELL_DIRECT_CONNECTED_RESPONSE, false));
                Log.i("reza", "2 = " + data
                        .getBooleanExtra(DeveloperInterface.TAPSELL_DIRECT_AVAILABLE_RESPONSE, false));
                Log.i("reza", "3 = " + data
                        .getIntExtra(DeveloperInterface.TAPSELL_DIRECT_AWARD_RESPONSE, 1));


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
