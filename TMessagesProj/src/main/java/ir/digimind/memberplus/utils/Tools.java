package ir.digimind.memberplus.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.io.File;
import java.util.ArrayList;

/**
 * AirPush
 * Created by Reza alidoust on 10/24/16.
 * AFE
 * rezaalidoust@live.com
 */

public class Tools {
    private static Context context;


    public static void shareApplication(Context context) {
        try {
            ArrayList<Uri> uris = new ArrayList<Uri>();
            Intent sendIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            sendIntent.setType("application/vnd.android.package-archive");
            uris.add(Uri.fromFile(new File(context.getApplicationInfo().publicSourceDir)));
            sendIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            context.startActivity(Intent.createChooser(sendIntent, null));


        } catch (Exception e) {
            //// TODO: 10/24/16 add fire base
            ArrayList<Uri> uris = new ArrayList<Uri>();
            Intent sendIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            sendIntent.setType("application/zip");
            uris.add(Uri.fromFile(new File(context.getApplicationInfo().publicSourceDir)));
            sendIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            context.startActivity(Intent.createChooser(sendIntent, null));
        }
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        Tools.context = context;
    }
}
