package ir.digimind.memberplus.utils;

import android.util.Log;

import org.telegram.messenger.AndroidUtilities;
import org.telegram.messenger.ChatObject;
import org.telegram.messenger.MessagesController;
import org.telegram.messenger.MessagesStorage;
import org.telegram.tgnet.ConnectionsManager;
import org.telegram.tgnet.RequestDelegate;
import org.telegram.tgnet.TLObject;
import org.telegram.tgnet.TLRPC;

import java.util.ArrayList;

import ir.digimind.memberplus.models.database.ChannelModel;

/**
 * Created by saeed on 11/6/2016.
 */
public class TL_Utile {
    private String Tag = "TL_Utile_tag";

    public interface onJoinComplete {
        public void onSuccess();

        public void onFail();
    }

    public interface onGetChatIdComplete {
        public void onSuccess(TLRPC.Chat chat, int id, String title);

        public void onFail();
    }

    public interface onLoadFullChatComplete {
        public void onSuccess(TLRPC.ChatFull chatFull);

        public void onFail();
    }

    public interface onGetChannelsComplete {
        public void onSuccess(ArrayList<TLRPC.Chat> chats);

        public void onFail();
    }


    public static void getChannelId(String username, final onGetChatIdComplete onGetChatIdComplete) {
//        String username = "almasgallery5090";
        if (username != null) {
            TLRPC.TL_contacts_resolveUsername req = new TLRPC.TL_contacts_resolveUsername();
            req.username = username;

            int requestId = ConnectionsManager.getInstance().sendRequest(req, new RequestDelegate() {
                @Override
                public void run(final TLObject response, final TLRPC.TL_error error) {
                    AndroidUtilities.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {

                            if (error == null) {
                                final TLRPC.TL_contacts_resolvedPeer res = (TLRPC.TL_contacts_resolvedPeer) response;
                                MessagesController.getInstance().putUsers(res.users, false);
                                MessagesController.getInstance().putChats(res.chats, false);
                                MessagesStorage.getInstance().putUsersAndChats(res.users, res.chats, false, true);

                                if (!res.chats.isEmpty()) {
                                    Log.i("chat_id", res.chats.get(0).id + "");
                                    Log.i("chat_title", res.chats.get(0).title + "");
                                    onGetChatIdComplete.onSuccess(res.chats.get(0), res.chats.get(0).id, res.chats.get(0).title);
                                } else {
                                    Log.i("user_id", res.users.get(0).id + "");
                                    onGetChatIdComplete.onFail();
                                }
                            } else {
                                onGetChatIdComplete.onFail();
                                Log.i("######", error.text);
                            }
                        }
                    });
                }
            });
        } else {
            onGetChatIdComplete.onFail();
        }
    }

    public static void joinChannel(int chatid, final onJoinComplete onComplete) {
//        String s = "1051161406";//channel id
        TLRPC.TL_channels_joinChannel req = new TLRPC.TL_channels_joinChannel();
        req.channel = MessagesController.getInputChannel(chatid);

        int requestId = ConnectionsManager.getInstance().sendRequest(req, new RequestDelegate() {
            @Override
            public void run(final TLObject response, final TLRPC.TL_error error) {
                AndroidUtilities.runOnUIThread(new Runnable() {
                    @Override
                    public void run() {

                        if (error == null) {
                            onComplete.onSuccess();
                        } else {
                            onComplete.onFail();
                        }
                    }
                });
            }
        });
    }


    public static void loadFullChat(int chat_id, final onLoadFullChatComplete onLoadFullChatComplete) {

        TLObject request;
        final TLRPC.Chat chat = MessagesController.getInstance().getChat(chat_id);
//        if (ChatObject.isChannel(chat_id)) {
        TLRPC.TL_channels_getFullChannel req = new TLRPC.TL_channels_getFullChannel();
        req.channel = MessagesController.getInputChannel(chat_id);
        request = req;
//        }
//        else {
//            TLRPC.TL_messages_getFullChat req = new TLRPC.TL_messages_getFullChat();
//            req.chat_id = chat_id;
//            request = req;
//        }
        final int reqId = ConnectionsManager.getInstance().sendRequest(request, new RequestDelegate() {
            @Override
            public void run(final TLObject response, final TLRPC.TL_error error) {
                AndroidUtilities.runOnUIThread(new Runnable() {
                    @Override
                    public void run() {
                        if (error == null) {
                            final TLRPC.TL_messages_chatFull res = (TLRPC.TL_messages_chatFull) response;
                            MessagesStorage.getInstance().putUsersAndChats(res.users, res.chats, true, true);
                            MessagesStorage.getInstance().updateChatInfo(res.full_chat, false);

                            if (ChatObject.isChannel(chat)) {

                                onLoadFullChatComplete.onSuccess(res.full_chat);

                            } else {

                                Log.e("======", "ChatObject. not isChannel");
                            }


                        } else {
                            onLoadFullChatComplete.onFail();
                            Log.i("############", error.text);
                        }
                    }
                });

            }
        });

    }

    public static void getChannels(final onGetChannelsComplete onGetChannelsComplete) {
        TLRPC.TL_messages_getDialogs req = new TLRPC.TL_messages_getDialogs();
        req.limit = 150;
        req.offset_peer = new TLRPC.TL_inputPeerEmpty();

        ConnectionsManager.getInstance().sendRequest(req, new RequestDelegate() {
            @Override
            public void run(TLObject response, final TLRPC.TL_error error) {


                final TLRPC.messages_Dialogs dialogsRes = (TLRPC.messages_Dialogs) response;
                AndroidUtilities.runOnUIThread(new Runnable() {
                    @Override
                    public void run() {
                        if (error == null) {
                            ArrayList<TLRPC.Chat> myChannels = new ArrayList<TLRPC.Chat>();
                            for (int i = 0; i < dialogsRes.chats.size(); i++) {

                                if (dialogsRes.chats.get(i).username != null) {
                                    if (!dialogsRes.chats.get(i).left) {
                                        ChannelModel channelModel = new ChannelModel();
                                        channelModel.setName(dialogsRes.chats.get(i).title);
                                        channelModel.setUserId(dialogsRes.chats.get(i).id + "");
                                        channelModel.setUsername(dialogsRes.chats.get(i).username);
//                                        channelModel.setImg("");
//                                        channelModel.setDescription("");
                                        myChannels.add(dialogsRes.chats.get(i));
                                    }
                                }
                            }
                            onGetChannelsComplete.onSuccess(myChannels);
                        } else {
                            onGetChannelsComplete.onFail();
                        }
                    }
                });

            }

        });
    }
}
