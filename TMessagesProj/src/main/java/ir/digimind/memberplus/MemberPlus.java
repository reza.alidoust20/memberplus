package ir.digimind.memberplus;

import android.app.Application;
import android.content.Context;

import com.ivankocijan.magicviews.MagicViews;

/**
 * AirPush
 * Created by Reza alidoust on 10/24/16.
 * AFE
 * rezaalidoust@live.com
 */

public class MemberPlus extends Application {
    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();

        appContext = getApplicationContext();
        MagicViews.setFontFolderPath(appContext, "fonts");


    }

    public static Context getAppContext() {
        return appContext;
    }

    public static void setAppContext(Context appContext) {
        MemberPlus.appContext = appContext;
    }
}
