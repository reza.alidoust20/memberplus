package ir.digimind.memberplus.views.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.telegram.messenger.R;

import java.util.List;

import ir.digimind.memberplus.models.view.NavigationItem;
import ir.digimind.memberplus.views.customs.RoundedImageView;


/**
 * ichat
 * Created by Reza alidoust on 10/19/16.
 * AFE
 * rezaalidoust@live.com
 */

public class NavigationAdapter extends ArrayAdapter<NavigationItem> {
    private Context context;
    private List<NavigationItem> values = null;
    private FragmentManager fragmentManager;
    Fragment fragment;

    public NavigationAdapter(Context context, List<NavigationItem> values, FragmentManager manager) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        fragmentManager = manager;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_navigation_view, parent, false);
        final TextView name = (TextView) rowView.findViewById(R.id.name);
        final RelativeLayout item = (RelativeLayout) rowView.findViewById(R.id.rel_nav_item);
        final RoundedImageView image = (RoundedImageView) rowView.findViewById(R.id.image);
        item.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    name.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    image.setBackgroundResource(R.drawable.ic_settings_yellow_900_24dp);
                } else {
                    name.setTextColor(context.getResources().getColor(R.color.colorNavigationPressed));
                    image.setBackgroundResource(values.get(position).getPic_id());
                }
            }
        });
        item.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    name.setTextColor(context.getResources().getColor(R.color.colorNavigationPressed));
                    image.setBackgroundResource(values.get(position).getPic_id());


                } else {
                    name.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    image.setBackgroundResource(values.get(position).getPic_id_pressed());
                }
                return false;
            }
        });

        item.setOnClickListener(values.get(position).getOnClickListener());

        name.setText(values.get(position).getTitle());
        image.setBackgroundResource(values.get(position).getPic_id());


        return rowView;
    }
}
