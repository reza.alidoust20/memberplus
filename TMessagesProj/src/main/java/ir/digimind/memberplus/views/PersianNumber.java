package ir.digimind.memberplus.views;

/**
 * AFE.Hiper
 * Created by Reza alidoust on 9/11/16.
 * AFE
 * rezaalidoust@live.com
 */

public class PersianNumber {
    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};


    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        String out = "";
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out += persianNumbers[number];
            } else {
                out += c;
            }


        }
        return out;
    }
}
