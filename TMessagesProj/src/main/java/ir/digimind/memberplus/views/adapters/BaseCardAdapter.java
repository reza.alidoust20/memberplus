package ir.digimind.memberplus.views.adapters;

import android.view.View;

/**
 * AirPush
 * Created by Reza alidoust on 10/27/16.
 * AFE
 * rezaalidoust@live.com
 */

public interface BaseCardAdapter<T> {



    public abstract int getCount();


    public abstract int getCardLayoutId();

    public abstract void onBindData(int position, View cardview);

//    public int getVisibleCardCount() {
//        return 3;
//    }
}
