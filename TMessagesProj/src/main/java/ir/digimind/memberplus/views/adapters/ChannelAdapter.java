package ir.digimind.memberplus.views.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ivankocijan.magicviews.views.MagicButton;
import com.ivankocijan.magicviews.views.MagicTextView;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.greenrobot.eventbus.EventBus;
import org.telegram.messenger.R;
import org.telegram.tgnet.TLRPC;
import org.telegram.ui.Components.AvatarDrawable;
import org.telegram.ui.Components.BackupImageView;

import java.util.List;

import co.uk.rushorm.core.RushSearch;
import ir.digimind.memberplus.models.buses.Coin;
import ir.digimind.memberplus.models.database.JoinedChannel;
import ir.digimind.memberplus.models.network.Channel;
import ir.digimind.memberplus.models.network.Response;
import ir.digimind.memberplus.models.network.User;
import ir.digimind.memberplus.network.NetworkService;
import ir.digimind.memberplus.utils.TL_Utile;
import ir.digimind.memberplus.views.customs.CustomSnackBar;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * AirPush
 * Created by Reza alidoust on 10/27/16.
 * AFE
 * rezaalidoust@live.com
 */

public class ChannelAdapter extends BaseAdapter {

    private KProgressHUD progressHUD;
    private List<Channel> data;
    private Context context;
    private AvatarDrawable avatarDrawable;


    public ChannelAdapter(Context context, List<Channel> data) {
        this.data = data;
        this.context = context;
        try {
            progressHUD = KProgressHUD.create(context)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("صبر کنید");
//                .setDetailsLabel("Downloading data");
            progressHUD.setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
        } catch (Exception o) {

        }
        avatarDrawable = new AvatarDrawable();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // normally use a viewholder
            v = inflater.inflate(R.layout.item_channel, parent, false);
        }
        MagicButton register = (MagicButton) v.findViewById(R.id.register);
        MagicButton report = (MagicButton) v.findViewById(R.id.report);
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                NetworkService.API().reportChannel(data.get(position).getUsername())
                        .enqueue(new Callback<Response>() {
                            @Override
                            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                if (response.body().isResult()) {

                                    CustomSnackBar snackBar = new CustomSnackBar(v, "گزارش شما ثبت شد", Snackbar.LENGTH_LONG, Color.YELLOW);
                                    snackBar.showSnackBar();
                                }
                            }

                            @Override
                            public void onFailure(Call<Response> call, Throwable t) {
                                CustomSnackBar snackBar = new CustomSnackBar(v, "مشکل در برقراری ارتباط با سرور", Snackbar.LENGTH_LONG, Color.YELLOW);
                                snackBar.showSnackBar();
                            }
                        });
            }
        });
        final MagicTextView channelName = (MagicTextView) v.findViewById(R.id.name);
        final MagicTextView channelDesc = (MagicTextView) v.findViewById(R.id.desc);
        final BackupImageView channelImg = (BackupImageView) v.findViewById(R.id.channelImg);

        channelDesc.setText("");
        channelName.setText("");
        TL_Utile.getChannelId(data.get(position).getUsername(), new TL_Utile.onGetChatIdComplete() {
            @Override
            public void onSuccess(TLRPC.Chat chat, int id, final String title) {
                data.get(position).setName(title);
                channelName.setText(title);
                setImageViewWithByteArray(channelImg, chat);
                TL_Utile.loadFullChat(id, new TL_Utile.onLoadFullChatComplete() {
                    @Override
                    public void onSuccess(TLRPC.ChatFull chatFull) {
                        data.get(position).setDesc(chatFull.about);
                        channelDesc.setText(chatFull.about);

                    }

                    @Override
                    public void onFail() {

                    }
                });
            }

            @Override
            public void onFail() {
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                try {
                    progressHUD.show();
                } catch (Exception e) {

                }
                TL_Utile.getChannelId(data.get(position).getUsername(), new TL_Utile.onGetChatIdComplete() {
                    @Override
                    public void onSuccess(TLRPC.Chat chat, int id, String title) {
                        TL_Utile.joinChannel(id, new TL_Utile.onJoinComplete() {
                            @Override
                            public void onSuccess() {
                                try {
                                    progressHUD.dismiss();
                                } catch (Exception e) {

                                }
                                JoinedChannel joinedChannel = new JoinedChannel();
                                joinedChannel.setTitle(data.get(position).getName());
                                joinedChannel.setDescription(data.get(position).getDesc());
                                joinedChannel.setUsername(data.get(position).getUsername());
                                List<JoinedChannel> joinedChannels = new RushSearch().whereEqual("username", data.get(position).getUsername()).limit(1)
                                        .find(JoinedChannel.class);
                                if (joinedChannels.size() > 0) {
                                    CustomSnackBar snackBar = new CustomSnackBar(v, "شما قبلا عضو این کانال شدید", Snackbar.LENGTH_LONG, Color.YELLOW);
                                    snackBar.showSnackBar();
                                } else {
                                    joinedChannel.save();
                                    User user = new User();
                                    TelephonyManager mngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

                                    user.setTel(mngr.getDeviceId());
                                    try {
                                        progressHUD.show();
                                    } catch (Exception e) {

                                    }
                                    NetworkService.API()
                                            .joinToChannel(data.get(position).getUsername(), user)
                                            .enqueue(new Callback<Response>() {
                                                @Override
                                                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                                    try {
                                                        progressHUD.dismiss();
                                                    } catch (Exception e) {

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<Response> call, Throwable t) {
                                                    try {
                                                        progressHUD.dismiss();
                                                    } catch (Exception e) {

                                                    }
                                                }
                                            });
                                    EventBus.getDefault().post(new Coin());
                                    CustomSnackBar snackBar = new CustomSnackBar(v, "شما عضو این کانال شدید", Snackbar.LENGTH_LONG, Color.YELLOW);
                                    snackBar.showSnackBar();
                                }


                            }

                            @Override
                            public void onFail() {
                                try {
                                    progressHUD.dismiss();
                                } catch (Exception e) {

                                }
                                CustomSnackBar snackBar = new CustomSnackBar(v, "خطا در عضویت در کانال", Snackbar.LENGTH_LONG, Color.YELLOW);
                                snackBar.showSnackBar();

                            }
                        });
                    }

                    @Override
                    public void onFail() {
                        try {
                            progressHUD.dismiss();
                        } catch (Exception e) {

                        }
                        CustomSnackBar snackBar = new CustomSnackBar(v, "خطا در دریافت آیدی کانال", Snackbar.LENGTH_LONG, Color.YELLOW);
                        snackBar.showSnackBar();

                    }
                });

            }
        });
        //// TODO: 10/27/16 channelname.setText();
        //// TODO: 10/27/16 cahnnelimg from url
//        Picasso.with(context).load(R.drawable.food).centerCrop().error(R.drawable.food).into(channelImg);

        return v;
    }

    public void setImageViewWithByteArray(BackupImageView imageView, TLRPC.Chat chat) {
        avatarDrawable.setInfo(chat);
        imageView.setImage(chat.photo.photo_small, "50_50", avatarDrawable);
    }

}
