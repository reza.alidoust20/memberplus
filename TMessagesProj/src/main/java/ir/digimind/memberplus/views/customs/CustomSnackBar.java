package ir.digimind.memberplus.views.customs;

import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

/**
 * ichat
 * Created by Reza alidoust on 10/20/16.
 * AFE
 * rezaalidoust@live.com
 */

public class CustomSnackBar {
    private View rootView;
    private String message;
    private int duration;
    private int color;

    public CustomSnackBar(View rootView, String message, int duration, int color) {
        this.rootView = rootView;
        this.message = message;
        this.duration = duration;
        this.color = color;
    }

    public void showSnackBar() {
        Snackbar snackbar = Snackbar
                .make(rootView, message, duration);


//// Changing message text color
//        snackbar.setActionTextColor(Color.RED);

// Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        Typeface typeface = Typeface.createFromAsset(rootView.getResources().getAssets(), "fonts/isans.ttf");
        textView.setTypeface(typeface);
        textView.setTextColor(color);
        textView.setGravity(Gravity.RIGHT);
        snackbar.show();


//        // Create the Snackbar
//        Snackbar snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG);
//// Get the Snackbar's layout view
//        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
//// Hide the text
////        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
////        textView.setVisibility(View.INVISIBLE);
//
//// Inflate our custom view
//        LayoutInflater inflater = (LayoutInflater) IChat.getAppContext()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View snackView = inflater.inflate(R.layout.snack_bar, null);
//// Configure the view
////        ImageView imageView = (ImageView) snackView.findViewById(R.id.image);
////        imageView.setImageBitmap(image);
//        MagicTextView textViewTop = (MagicTextView) snackView.findViewById(R.id.snackbar_text);
//        textViewTop.setText("برو یره");
//        textViewTop.setTextColor(color);
//
//// Add the view to the Snackbar's layout
//        layout.addView(snackView, 0);
//// Show the Snackbar
//        snackbar.show();
    }
}
