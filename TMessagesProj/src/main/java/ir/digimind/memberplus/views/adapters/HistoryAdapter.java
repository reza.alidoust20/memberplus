package ir.digimind.memberplus.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ivankocijan.magicviews.views.MagicTextView;

import org.telegram.messenger.R;
import org.telegram.tgnet.TLRPC;
import org.telegram.ui.Components.AvatarDrawable;
import org.telegram.ui.Components.BackupImageView;

import java.util.List;

import ir.digimind.memberplus.models.database.MyChannel;
import ir.digimind.memberplus.utils.TL_Utile;
import ir.digimind.memberplus.views.PersianNumber;


/**
 * AirPush
 * Created by Reza alidoust on 10/26/16.
 * AFE
 * rezaalidoust@live.com
 */

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final int TYPE_CHANNEL = 0;
    public final int TYPE_LOAD = 1;
    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;
    private static AvatarDrawable avatarDrawable;



    private List<MyChannel> itemList;
    private Context context;

    public HistoryAdapter(Context context, List<MyChannel> itemList) {
        this.itemList = itemList;
        this.context = context;
        avatarDrawable = new AvatarDrawable();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == TYPE_CHANNEL) {
            return new ChannelHolder(inflater.inflate(R.layout.item_card_view, parent, false));
        } else {
            return new LoadHolder(inflater.inflate(R.layout.item_load_more, parent, false));
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if (getItemViewType(position) == TYPE_CHANNEL) {
            ((ChannelHolder) holder).bindData(itemList.get(position));
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (itemList.get(position).getUsername() != null && !itemList.get(position).getUsername().equals("")) {
            return TYPE_CHANNEL;
        } else {
            return TYPE_LOAD;
        }
    }


    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    static class ChannelHolder extends RecyclerView.ViewHolder {

        public MagicTextView name;
        public MagicTextView star;
        public BackupImageView channelImg ;



        public ChannelHolder(View view) {
            super(view);
            name = (MagicTextView) view.findViewById(R.id.name);
            star = (MagicTextView) view.findViewById(R.id.star);
            channelImg = (BackupImageView) view.findViewById(R.id.channel_photo);

        }

        void bindData(MyChannel channelModel) {
            name.setText(channelModel.getTitle());
            star.setText(PersianNumber
                    .toPersianNumber(Integer.toString(channelModel.getNumber())));

            TL_Utile.getChannelId(channelModel.getUsername(), new TL_Utile.onGetChatIdComplete() {
                @Override
                public void onSuccess(TLRPC.Chat chat, int id, final String title) {
                    avatarDrawable.setInfo(chat);
                    channelImg.setImage(chat.photo.photo_small, "50_50", avatarDrawable);

                }

                @Override
                public void onFail() {
                }
            });
        }
    }

    static class LoadHolder extends RecyclerView.ViewHolder {
        public LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }


    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}